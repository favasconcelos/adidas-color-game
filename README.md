# Color Game

[![pipeline status](https://gitlab.com/felipe-projects/adidas-color-game/badges/dev/pipeline.svg)](https://gitlab.com/felipe-projects/adidas-color-game/commits/dev)

> Implement the color game and a robot to solve it automatically.
>
> Color game explanation with live demo: brainmetrix.com/color-game

---

## Game Config:

Change `src/config.js`, where you will find:

-   **COLORS**: The number of possible answers.
-   **NUMBER_OF_QUESTIONS**: That defines the number of questions the user must answer.
-   **GAME_LEVELS**: The differents game levels, each one with a set of configs as:
    -   **Timer**: The time the will will have to answer the question.
    -   **Options**: The number of generated answers.
    -   **Points**:
        -   **Question**: The number of points per right answer.
        -   **Time**: The number of points per time remaining.

---

## Build and Run:

**Install**: `yarn`

**Start webserver**: `yarn start`

**Build for production**: `yarn build`

---

## Mandatory tasks:

-   Color Game:
    -   It shows one color name but with different text color and two color
        names below (with different text colors too).
-   The game is finished after 10 rounds with a simple message on the screen.
-   There are two modes (_human_ and _robot_):
    -   The human mode allows the user to play.
    -   The robot move resolves the game automatically
-   A timeout can be set to be able to see how it automatically plays.
-   It should act like a human, analyzing the DOM to find the correct
    response.
-   Documentation.

---

## Optional tasks:

-   Countdown for each question with either a progress bar or a similar
    component.
-   Show the score in the message.
-   The score depends on the remaining time after choosing the color.
    Different levels of difficulty regarding available time to pick the color and the
    number of available colors.
    Tests automation: unit, integration, functional.
    o Unit/integration just execute the JavaScript code.
    o Functional tests are executed in a real browser and used the DOM to
    check the expectations.

---

## Tooling:

-   Language: ES5, ES6 or Typescript.
-   Style: CSS, SCSS or LESS.
-   A JavaScript framework can be used (preferably: React, Vue, Angular(JS)).
-   Utilities library or DOM management library can be added.
