const initalState = { config: null, questions: null };

export default (state = initalState, action) => {
    switch (action.type) {
        case 'START_GAME':
            return { ...state, config: action.payload, questions: null };
        case 'END_GAME':
            return { ...state, questions: action.payload };
        case 'RESET_GAME':
            return { ...initalState };
        default:
            return state;
    }
};
