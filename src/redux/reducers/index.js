// global
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
// local
import game from './game';

const rootReducer = history =>
    combineReducers({
        game,
        router: connectRouter(history),
    });

export default rootReducer;
