// global
import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import { createMemoryHistory } from 'history';
// middlewares
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

// local
import rootReducer from './reducers';

// Create an enhanced history that syncs navigation events with the store
export const history = createMemoryHistory();

// mount the middlewares
const middlewares = [];
middlewares.push(routerMiddleware(history));
middlewares.push(thunk);

// only add the log if running on development
if (process.env.NODE_ENV === 'development') {
    middlewares.push(createLogger({ collapsed: true, duration: true, diff: true }));
}

// create the store
export default createStore(rootReducer(history), compose(applyMiddleware(...middlewares)));
