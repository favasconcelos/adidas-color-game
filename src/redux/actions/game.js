// global
import { replace } from 'connected-react-router';
// local

const _resetGame = () => ({ type: 'RESET_GAME' });
const _startGame = config => ({ type: 'START_GAME', payload: config });
const _endGame = questions => ({ type: 'END_GAME', payload: questions });

export const resetGame = () => dispatch => {
    dispatch(_resetGame());
    dispatch(replace('/'));
};

export const startGame = config => dispatch => {
    dispatch(_startGame(config));
    dispatch(replace('/game'));
};

export const endGame = questions => dispatch => {
    dispatch(_endGame(questions));
    dispatch(replace('/score'));
};
