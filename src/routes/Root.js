import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
// local
import store, { history } from '../redux/store';
// routes
import Home from './Home';
import Game from './Game';
import Score from './Score';

class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route path="/" exact component={Home} />
                        <Route path="/game" exact component={Game} />
                        <Route path="/score" exact component={Score} />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        );
    }
}

export default Root;
