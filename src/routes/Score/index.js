// global
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, List, Header, Button } from 'semantic-ui-react';
import { connect } from 'react-redux';
// local
import './Score.scss';
import GameHeader from '../../components/GameHeader';
// actions
import { startGame, resetGame } from '../../redux/actions/game';
// helper
import { capitalize } from '../../utils';
import { GAME_LEVELS } from '../../config';

class Score extends Component {
    state = { totalScore: this.calculateTotalScore() };

    componentDidMount() {
        const { config } = this.props.game;
        if (!config) {
            this.props.dispatch(resetGame());
        }
    }

    /**
     * The score will be calculated as follows:
     * Points per question + (Time Remaining * Points per time)
     */
    calculateTotalScore() {
        const { questions, config } = this.props.game;
        if (config) {
            const { timer, points } = GAME_LEVELS[config.level];
            return questions.filter(q => q.score.isCorrect === true).reduce((acc, current) => {
                const t = (timer - current.score.timeSpent) * points.time;
                return acc + points.question + t;
            }, 0);
        } else {
            return 0;
        }
    }

    handleHome = () => this.props.dispatch(resetGame());

    handlePlayAgain = () => {
        const { config } = this.props.game;
        this.props.dispatch(startGame(config));
    };

    render() {
        const { config, questions } = this.props.game;
        // if there is no config, render nothing
        if (!config) return null;

        return (
            <Container id="score">
                <GameHeader />
                <Header as="h2">
                    Final Score [{this.state.totalScore} points]
                    <Header.Subheader>
                        <strong>Mode:</strong> {config.gameMode} | <strong>Level:</strong> {config.level}
                    </Header.Subheader>
                </Header>
                <List>
                    {questions.map((question, index) => {
                        const [iconName, color] = question.score.isCorrect ? ['check', 'green'] : ['close', 'red'];
                        return (
                            <List.Item key={`answer-${index}`}>
                                <List.Icon name={iconName} color={color} verticalAlign="middle" />
                                <List.Content>
                                    <List.Header>Question: {capitalize(question.title.color)}</List.Header>
                                    <List.Description>
                                        <strong>Answer:</strong> {question.score.answer} | <strong>Time:</strong> {question.score.timeSpent}
                                    </List.Description>
                                </List.Content>
                            </List.Item>
                        );
                    })}
                </List>
                <Button content="Play Again!" onClick={this.handlePlayAgain} primary />
                <Button content="Home" onClick={this.handleHome} secondary />
            </Container>
        );
    }
}

Score.propTypes = {
    dispatch: PropTypes.any.isRequired,
    game: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({ game: state.game });
export default connect(mapStateToProps)(Score);
