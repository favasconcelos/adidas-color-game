// global
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Header, Divider, Button, Progress } from 'semantic-ui-react';
import { connect } from 'react-redux';
// local
import './Game.scss';
// components
import GameHeader from '../../components/GameHeader';
// actions
import { endGame, resetGame } from '../../redux/actions/game';
// helper
import { shuffleArray, pickFromArray, capitalize } from '../../utils';
import { COLORS, GAME_LEVELS, NUMBER_OF_QUESTIONS } from '../../config';

function runRobot() {
    const title = document.querySelector('#title');
    const answer = title.dataset.color.toLowerCase();
    const options = Array.from(document.querySelectorAll('button.option'));
    for (let option of options) {
        const color = option.dataset.color.toLowerCase();
        if (color === answer) {
            option.classList.add('mark-as-answer');
            setTimeout(() => {
                option.click();
            }, 1000);
        }
    }
}

class Game extends Component {
    state = { qIndex: 0, isReady: false, timeRamaining: 0 };

    componentDidMount() {
        const { config } = this.props.game;
        if (!config) {
            this.props.dispatch(resetGame());
        } else {
            const { gameMode, level } = config;
            const isRobot = gameMode === 'robot';
            const levelConfig = GAME_LEVELS[level];
            const questions = this.initQuestions(level);
            this.setState({ isRobot, isReady: true, questions, levelConfig, timeSpent: 0 });
            this.startTimeout();
        }
    }

    componentWillUnmount() {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
    }

    initQuestions(level) {
        const numberOfOptions = GAME_LEVELS[level].options;
        // shuffle the colors array
        const allColors = shuffleArray(COLORS);
        // get only NUMBER_OF_QUESTIONS colors from the array
        const colors = allColors.slice(0, NUMBER_OF_QUESTIONS);
        return colors.map(answer => {
            // pick a color from all colors that is not the answer
            const titleText = pickFromArray(allColors, [answer]);
            // create the title
            const title = { text: capitalize(titleText), color: answer };
            // init the options
            const options = this.initOptions(answer, allColors, numberOfOptions);
            return { title, options, score: { timeRamaining: 0, isCorrect: false } };
        });
    }

    /**
     *
     * @param {String} answer the right color.
     * @param {Array} allColors all the possible colors.
     * @param {*} numberOfOptions
     */
    initOptions(answer, allColors, numberOfOptions) {
        // get all the colors that are not the answer, shuffle and select only the right number
        let possibleValues = shuffleArray(allColors.filter(o => o !== answer)).slice(0, numberOfOptions - 1);
        // shuffle one more time since we always add the answer in the first position
        possibleValues = shuffleArray([answer, ...possibleValues]);
        // helper array to keep track of what colors where used so we do not repeat them
        const alreadyUsed = [];
        return possibleValues.map(a => {
            // pick a color that were not used yet
            const color = pickFromArray(allColors, alreadyUsed);
            alreadyUsed.push(color);
            return { text: capitalize(a), color };
        });
    }

    updateTimeProgress = () => {
        const { isRobot, timeSpent, levelConfig } = this.state;
        if (isRobot) {
            clearTimeout(this.timeout);
            runRobot();
        }
        if (timeSpent === levelConfig.timer) {
            this.handleOption();
        } else {
            this.setState({ timeSpent: timeSpent + 1 });
            this.startTimeout();
        }
    };

    handleOption = (color = '') => {
        const { qIndex, questions, timeSpent } = this.state;
        const isCorrect = questions[qIndex].title.color.toLowerCase() === color.toLowerCase();
        questions[qIndex].score = {
            isCorrect,
            timeSpent,
            answer: color,
        };
        if (qIndex === NUMBER_OF_QUESTIONS - 1) {
            this.props.dispatch(endGame(questions));
        } else {
            this.startTimeout();
            this.setState({ qIndex: qIndex + 1, questions, timeSpent: 0 });
        }
    };

    startTimeout() {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(this.updateTimeProgress, 1000);
    }

    render() {
        // if there is no config, render nothing
        if (!this.props.game.config) return null;

        const { isReady } = this.state;

        return (
            <Container id="game">
                <GameHeader />
                <Divider />
                {isReady ? this.renderGame() : this.renderLoading()}
            </Container>
        );
    }

    renderLoading() {
        return 'Loading...';
    }

    renderGame() {
        const { qIndex, questions, timeSpent, levelConfig } = this.state;
        const timeProgress = (timeSpent * 100) / levelConfig.timer;
        return <Question qIndex={qIndex} question={questions[qIndex]} handleOption={this.handleOption} timeProgress={timeProgress} />;
    }
}

Game.propTypes = {
    dispatch: PropTypes.any.isRequired,
    game: PropTypes.object.isRequired,
};

function Question({ qIndex, question, handleOption, timeProgress }) {
    return (
        <Container id="question">
            <Container id="question-progress" textAlign="right">
                Question {qIndex + 1} / {NUMBER_OF_QUESTIONS}
            </Container>
            <Container textAlign="center">
                <Header as="h1" id="title" color={question.title.color} data-color={question.title.color}>
                    {question.title.text}
                </Header>
                <Progress percent={timeProgress} size="tiny" active />
                {question.options.map((o, index) => {
                    const key = `q-${qIndex}-o-${index}`;
                    return (
                        <Button className="option" key={key} color={o.color} onClick={handleOption.bind(this, o.text)} data-color={o.text} basic>
                            {o.text}
                        </Button>
                    );
                })}
            </Container>
        </Container>
    );
}

Question.propTypes = {
    qIndex: PropTypes.number.isRequired,
    question: PropTypes.object.isRequired,
    handleOption: PropTypes.func.isRequired,
    timeProgress: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({ game: state.game });
export default connect(mapStateToProps)(Game);
