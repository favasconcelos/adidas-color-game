// global
import React, { Component } from 'react';
import { Container, Divider } from 'semantic-ui-react';
// local
import GameHeader from '../../components/GameHeader';
import GameForm from '../../components/GameForm';

class Home extends Component {
    render() {
        return (
            <Container id="home">
                <GameHeader />
                <p>
                    This is a great game to keep your mind sharp and slow down the effect of Alzheimer's disease. This is a brain test that uses colors and mind tricks to force you
                    to concentrate and give the correct answer within four seconds. <a href="http://brainmetrix.com/color-game">(http://brainmetrix.com/color-game)</a>
                </p>
                <Divider />
                <GameForm />
            </Container>
        );
    }
}

export default Home;
