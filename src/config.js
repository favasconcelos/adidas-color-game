/**
 * Possible Colors
 */
export const COLORS = ['red', 'orange', 'yellow', 'green', 'teal', 'blue', 'violet', 'purple', 'pink', 'brown', 'grey', 'black'];

/**
 * Number of questions.
 */
export const NUMBER_OF_QUESTIONS = 10;

/**
 * Levels
 */
export const GAME_LEVELS = {
    easy: { timer: 10, options: 2, points: { question: 2, time: 10 } },
    medium: { timer: 8, options: 4, points: { question: 4, time: 12.5 } },
    hard: { timer: 5, options: 6, points: { question: 6, time: 20 } },
};
