import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Radio, Button, Divider } from 'semantic-ui-react';
import { connect } from 'react-redux';
// local
import './GameForm.scss';
import { GAME_LEVELS } from '../../config';
import { capitalize } from '../../utils';
import { startGame } from '../../redux/actions/game';

/**
 * Tipe of game modes.
 */
const GAME_MODES = ['human', 'robot'];

function GameMode({ value, currentValue, onChange, ...args }) {
    return <Radio label={capitalize(value)} value={value} onChange={onChange} name="gameMode" checked={currentValue === value} {...args} />;
}
GameMode.propTypes = {
    value: PropTypes.oneOf(GAME_MODES).isRequired,
    currentValue: PropTypes.oneOf(GAME_MODES).isRequired,
    onChange: PropTypes.func.isRequired,
};

function GameLevel({ value, currentValue, onChange, ...args }) {
    return <Radio label={capitalize(value)} value={value} onChange={onChange} name="level" checked={currentValue === value} {...args} />;
}
GameLevel.propTypes = {
    value: PropTypes.oneOf(Object.keys(GAME_LEVELS)).isRequired,
    currentValue: PropTypes.oneOf(Object.keys(GAME_LEVELS)).isRequired,
    onChange: PropTypes.func.isRequired,
};

class GameForm extends Component {
    state = { gameMode: 'human', level: 'easy' };

    changeGameMode = (e, { value }) => this.setState({ gameMode: value });
    changeLevel = (e, { value }) => this.setState({ level: value });
    startGame = e => {
        e.preventDefault();
        this.props.dispatch(startGame(this.state));
    };

    render() {
        return (
            <Form>
                <Form.Field>
                    <label>Mode</label>
                    <GameMode value="human" currentValue={this.state.gameMode} onChange={this.changeGameMode} />
                    <GameMode value="robot" currentValue={this.state.gameMode} onChange={this.changeGameMode} />
                </Form.Field>
                <Form.Field>
                    <label>Level</label>
                    {Object.keys(GAME_LEVELS).map(level => {
                        return <GameLevel key={level} value={level} currentValue={this.state.level} onChange={this.changeLevel} />;
                    })}
                </Form.Field>
                <Divider />
                <Form.Field control={Button} primary onClick={this.startGame}>
                    Play
                </Form.Field>
            </Form>
        );
    }
}

GameForm.propTypes = {
    dispatch: PropTypes.any,
};

export default connect()(GameForm);
