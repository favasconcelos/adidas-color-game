// global
import React from 'react';
import { Header } from 'semantic-ui-react';
// local
import './GameHeader.scss';

export default function GameHeader() {
    return (
        <Header id="game-header" as="h1" block>
            <span style={{ color: 'red' }}>C</span>
            <span style={{ color: 'blue' }}>o</span>
            <span style={{ color: 'green' }}>l</span>
            <span style={{ color: 'yellow' }}>o</span>
            <span style={{ color: 'purple' }}>r</span> Game
        </Header>
    );
}
