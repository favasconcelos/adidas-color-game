import React from 'react';
import ReactDOM from 'react-dom';

import './index.scss';
import 'semantic-ui-css/semantic.min.css';

import Root from './routes/Root';

ReactDOM.render(<Root />, document.getElementById('root'));
