/**
 * Helper method used to shuffle an array.
 * Source: https://gist.github.com/guilhermepontes/17ae0cc71fa2b13ea8c20c94c5c35dc4
 * @param {Array} array array to be shuffled.
 */
export const shuffleArray = array =>
    array
        .map(a => [Math.random(), a])
        .sort((a, b) => a[0] - b[0])
        .map(a => a[1]);

/**
 *
 * @param {*} array
 * @param {*} color
 */
export const pickFromArray = (array, colors = []) => {
    array = array.filter(c => colors.indexOf(c) === -1);
    return array[Math.floor(Math.random() * array.length)];
};

/**
 * Helper function to convert the value to a label with the first letter uppercased.
 * @param {String} value
 */
export const capitalize = value => value.charAt(0).toUpperCase() + value.slice(1);
